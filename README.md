# Ismail Starter CMS

This is CMS project starter based on Laravel 5.7

# Installation
### 1. Manual Install
you can manually install by cloning this repo or download the zip file from this repo, and run composer install.
```sh
$ git clone https://gitlab.com/sharkkyo/ismail-starter-cms.git
$ cd ismail-starter-cms
$ composer install
```

### 2. Environment Files
This package ships with a .env.example file in the root of the project. You must rename this file to just .env

### 3. Create Database
You must create your database on your server and on your .env file update the following lines:
> DB_CONNECTION=mysql
> DB_HOST=127.0.0.1
> DB_PORT=3306
> DB_DATABASE=homestead
> DB_USERNAME=homestead
> DB_PASSWORD=secret

### 4. Artisan Commands
The first thing we are going to so is set the key that Laravel will use when doing encryption.
```sh
php artisan key:generate
```
It's time to see if your database credentials are correct. We are going to run the built in migrations to create the database tables:
```sh
php artisan migrate
```
Now seed the database with:
```sh
php artisan db:seed
```

### 5. Storage Link
After your project is installed you must run this command to link your public storage folder for user avatar uploads:
```sh
php artisan storage:link
```

### 6. Login to CMS
After your project is installed and you can access it in a browser, click the login button on the right of the navigation bar.

The administrator credentials are:
>Username: admin@gmail.com
>Password: lingling


